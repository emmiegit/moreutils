/*
 * info.h
 *
 * rmln - Only remove links.
 * Copyright (c) 2016 Ammon Smith
 *
 * rmln is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * rmln is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rmln.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __INFO_H
#define __INFO_H

#define PROGRAM_NAME			"rmln"
#define PROGRAM_VERSION			"0.1.0"
#define PROGRAM_AUTHORS			"Ammon Smith"
#define PROGRAM_YEARS           	"2016"

#ifndef GIT_HASH
# define GIT_HASH			"nogithash"
#endif /* GIT_HASH */

#endif /* __INFO_H */

